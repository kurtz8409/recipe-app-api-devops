#!/bin/sh

set -e 

# Django setup:
# 1. collect all static files and copy into a single directory 
#    (set noinput so to answer yes to all prompt questions)
python manage.py collectstatic --noinput

# 2. migrate the database so changes are reflected in the database
python manage.py wait_for_db  # a custom utility to wait for DB to setup
python manage.py migrate

# 3. run the uWSGI server
uwsgi --socket :9000 --workers 4 --master --enable-threads --module app.wsgi
