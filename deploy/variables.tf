variable "prefix" {
  default = "uat"
}

variable "project" {
  default = "recipe-app-api-devops"
}

variable "contact" {
  default = "email@dev.com"
}
